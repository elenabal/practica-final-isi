**Detector de Copias**

Programa en Java que detecta la copia entre dos practicas de dos alumnos guardados en la base de datos. El programa tiene varias condiciones:

- Cada alumno tendrá guardado su nombre, apellido y su DNI.
- Las practicas estaran compuestas por el DNI del alumno y el enlace al repositorio con la practica.
- Los alumnos solo pueden entregar archivos .txt y .java. Y en dichos repositorios no debe haber ninguna extensión del tipo '.zip' , '.jar'.
- Primero el programa imprimirá una lista de los commits de ambas practicas y el último commit que ha hecho cada alumno.
- El indice de copia entre dos prácticas está establecido en el 60% de similitud.
- En caso de que haya copia, el programa avisará de que hay sospecha de copia.
- En caso de que no haya copia, el programa avisará de que no hay sospecha de copia.
- Para ambos casos, si se encuentran diferencias entre dichos ficheros, se mostrará el informe de diferencias.
- La App de prueba ha sido diseñada para comparar dos practicas .txt y .java de cuatro alumnos diferentes. Los alumnos son Manolo y Sergio para el .txt y Pablo y Bea para el .java. 
