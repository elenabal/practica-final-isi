DROP TABLE IF EXISTS alumnos;
CREATE TABLE alumnos (
nombre VARCHAR(30),
apellidos VARCHAR(50),
dni VARCHAR(10) PRIMARY KEY);

DROP TABLE IF EXISTS practica;
CREATE TABLE practica (
url VARCHAR (512) PRIMARY KEY,
dni VARCHAR(10),
FOREIGN KEY (dni) REFERENCES alumnos(dni));

INSERT INTO alumnos VALUES ('Elena', 'Ballesteros','012345678B');
INSERT INTO alumnos VALUES ('Lorena', 'Alvarez','567984234T');
INSERT INTO alumnos VALUES ('Jesus', 'Fernandez','567345213P');

INSERT INTO practica VALUES ('https://gitlab.etsit.urjc.es/elenabal/django-cms-css','012345678B');
INSERT INTO practica VALUES ('https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas','567984234T');
INSERT INTO practica VALUES ('https://gitlab.etsit.urjc.es/elenabal/final-lovisto','567345213P');
