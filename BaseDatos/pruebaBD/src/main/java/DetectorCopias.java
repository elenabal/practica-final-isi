import java.io.File;
import java.util.ArrayList;
import java.io.*;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.api.Git;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import java.io.IOException;

import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.EditList;
import org.eclipse.jgit.diff.HistogramDiff;
import org.eclipse.jgit.diff.RawText;
import org.eclipse.jgit.diff.RawTextComparator;

public class DetectorCopias{
	public static Git clonar(String url, String directory) throws IOException,InvalidRemoteException, TransportException, 							GitAPIException{
		Git git = Git.cloneRepository()
	   		 .setURI(url)
	  		 .setDirectory(new File(directory))
	   		 .call();
	   	return git;
	}
	
	public static Repository getRepo(Git git){
		return git.getRepository();
	}
	
	public static int contarCommits(Git git) throws IOException, InvalidRemoteException, TransportException,GitAPIException {
		Iterable<RevCommit> commits = git.log().all().call();
                int counter = 0;
                for (RevCommit commit : commits) {
                    System.out.println("LogCommit: " + commit);
                    counter++;
                }
                return counter;
	}
	
	public static String getLastCommit(Git git) throws IOException, InvalidRemoteException, TransportException,GitAPIException{
		Iterable<RevCommit> commits = git.log().all().call();
		String ultimocommit = commits.iterator().next().toString();
		return ultimocommit;
	}
	
	public static ArrayList<File> listarFicheros(File directory){
		//¿Cómo saber el directorio donde esta git sin tener que meterlo a mano?
		ArrayList<File> files = new ArrayList<File>();
		File[] ficheros = directory.listFiles();
		
		if(ficheros==null || ficheros.length == 0){
			files=null;
		}else{
			for(int i=0;i<ficheros.length;i++){
				if(ficheros[i].isDirectory()){
					if(!ficheros[i].getName().equals(".git")){
						//if(ficheros[i].getName().contains(".jar")){ //Sigue detectando los .jar
							File[] ficheros2 = ficheros[i].listFiles();
							for(int j=0;j<ficheros2.length;j++){
								files.add(ficheros2[j]);
							}	
						//}				
					}
				}else{
					files.add(ficheros[i]);
				}
			}
		}
		return files;
	}
	
	public static int contarLineas(File fichero){
		int numlineas = 0;
		
		try{
   			BufferedReader fich = new BufferedReader(new FileReader(fichero));
   			String linea = "";
          	 	while((linea = fich.readLine()) != null){
              			numlineas++;
            		}
       	}catch (Exception e){ //Catch de excepciones
            		System.err.println("Ocurrio un error: " + e.getMessage());
        	}
        	return numlineas;
	}
	
	public static boolean difLineas(File fich1, File fich2){
		int numlineasfich1;
		int numlineasfich2;
		int numlineasiguales=0;
		boolean copia = false;
		try{
			BufferedReader qlee1 = new BufferedReader(new FileReader(fich1));
			BufferedReader qlee2 = new BufferedReader(new FileReader(fich2));
			String l1="";
			String l2="";
			while((l1 = qlee1.readLine())!=null && (l2=qlee2.readLine())!=null){
				if(l1.equals(l2)){
					numlineasiguales++;
				}
				
			}
		}catch (Exception e){ //Catch de excepciones
            		System.err.println("Ocurrio un error: " + e.getMessage());
        	}
        	
        	numlineasfich1 = contarLineas(fich1);
        	numlineasfich2 = contarLineas(fich2);
        	if(((numlineasiguales/numlineasfich1)*100) > 60  || ((numlineasiguales/numlineasfich2)*100) > 60){
        		copia = true;
        		System.out.println("Hay probabilidad de tener copia");
        	}else{
        		System.out.println("No hay probabilidad de tener copia");
        	}
        	
        	return copia;
	}
	
	
	public static String getDiff(File file1, File file2) {
	    OutputStream out = new ByteArrayOutputStream();
	    try {
	        RawText rt1 = new RawText(file1);
	        RawText rt2 = new RawText(file2);
	        EditList diffList = new EditList();	
	        diffList.addAll(new HistogramDiff().diff(RawTextComparator.DEFAULT, rt1, rt2));
	        DiffFormatter diffFormatter = new DiffFormatter(out);
	        diffFormatter.format(diffList, rt1, rt2);
	        System.out.println(out.toString());
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return out.toString();
	}
}
