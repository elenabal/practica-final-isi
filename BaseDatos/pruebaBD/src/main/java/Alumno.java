import java.util.List;

public class Alumno {

	private String nombre;
	private String apellidos;
	private String dni;

	/*Constructor de Alumno */
	public Alumno(String nombre, String apellidos, String dni){
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
	}
	
	/*Metodo para obtener nombre del alumno */
	public String getNombre() {
		return nombre;
	}
	
	/*Metodo para obtener apellidos del alumno */
	public String getApellidos() {
		return apellidos;
	}
	
	/*Metodo para obtener el dni del alumno */
	public String getDni() {
		return dni;
	}
	
	/*Buscar si el alumno esta en la lista */
	public static boolean searchAlumno(String nombre, List lista){
		int x = 0;
		boolean found = false;
		int dim = lista.size();
		
		while(found != true && x < dim){
		Alumno n_name = (Alumno)lista.get(x);
			if(n_name.getNombre().equals(nombre)){
				found = true;
			}else{
				x++;
			}
		}
		
		return found;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellidos == null) ? 0 : apellidos.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumno other = (Alumno) obj;
		if (apellidos == null) {
			if (other.apellidos != null)
				return false;
		} else if (!apellidos.equals(other.apellidos))
			return false;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
}


