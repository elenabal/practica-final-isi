import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AlumnoDao {
    private static Connection c;
    
    /* Método para iniciar la conexión con la base de datos */
    public void openConnection() throws SQLException{
    	c = DriverManager.getConnection("jdbc:sqlite:entregas.sql"); /*Me conecto con mi base de datos */
    	c.setAutoCommit(false); 	
    }
    
    /* Cierre con la base de datos */
    public void closeConnection(PreparedStatement ps, ResultSet rs) throws SQLException{
        try {
            if(rs!=null){
            	rs.close();
            }
            ps.close();
            c.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public AlumnoDao() {
        try {
            if(c!=null) return;
            	openConnection();
            c.prepareStatement("drop table if exists alumnos").execute();
            c.prepareStatement("create table alumnos (nombre varchar(30), apellidos varchar(50), dni varchar(10))").execute(); 
            c.commit(); 
            c.close();
	
        } catch (SQLException e) { /* Manejador de excepciones */
            throw new RuntimeException(e);
        }
    }

    /* Metodo all */
    public List<Alumno> all() {

        List<Alumno> allAlumnos = new ArrayList<Alumno>();

        try {
            openConnection();
            PreparedStatement ps = c.prepareStatement("select * from alumnos"); /*Pido todas las filas de la tabla */

            ResultSet rs = ps.executeQuery(); 

            while(rs.next()) {
                String nombre = rs.getString("nombre");
                String apellidos = rs.getString("apellidos");
                String dni = rs.getString("dni");
                allAlumnos.add(new Alumno(nombre, apellidos, dni));
            }
            closeConnection(ps,rs);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            return allAlumnos;
        }
    }
	
    public void save(Alumno alum) {
        try {
            openConnection();
            PreparedStatement ps = c.prepareStatement("insert into alumnos (nombre, apellidos, dni) values (?,?,?)"); 
            ps.setString(1, alum.getNombre()); 
            ps.setString(2, alum.getApellidos()); 
            ps.setString(3, alum.getDni()); 
            
            ps.execute();

            c.commit();
            closeConnection(ps,null);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

