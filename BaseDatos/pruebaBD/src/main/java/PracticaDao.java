import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PracticaDao {
	private static Connection c;
	
    /* Método para iniciar la conexión con la base de datos */
    public void openConnection() throws SQLException{
    	c = DriverManager.getConnection("jdbc:sqlite:entregas.sql"); /*Me conecto con mi base de datos */
    	c.setAutoCommit(false); 	
    }
    
    /* Cierre con la base de datos */
    public void closeConnection(PreparedStatement ps, ResultSet rs) throws SQLException{
        try {
            if(rs!=null){
            	rs.close();
            }
            ps.close();
            c.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    public PracticaDao() {
        try {
            if(c!=null) return;
            openConnection();

            c.prepareStatement("drop table if exists practica").execute();
            c.prepareStatement("create table practica (url varchar(512), dni varchar(10))").execute(); 
            c.commit(); 
            c.close();
	
        } catch (SQLException e) { /* Manejador de excepciones */
            throw new RuntimeException(e);
        }
    }

    /* Metodo all */
    public List<Practica> all() {

        List<Practica> allPracticas = new ArrayList<Practica>();

        try {
            openConnection();
            PreparedStatement ps = c.prepareStatement("select * from practica"); 

            ResultSet rs = ps.executeQuery(); 

            while(rs.next()) {
                String url = rs.getString("url");
                String dni = rs.getString("dni");
                allPracticas.add(new Practica(url, dni)); 
            }
            closeConnection(ps,rs);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            return allPracticas;
        }
    }

    public void save(Practica prac) {
        try {
            openConnection();
            PreparedStatement ps = c.prepareStatement("insert into practica (url, dni) values (?,?)");
            ps.setString(1, prac.getUrl()); 
            ps.setString(2, prac.getDni()); 
            
            ps.execute();

            c.commit();
            closeConnection(ps, null);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

