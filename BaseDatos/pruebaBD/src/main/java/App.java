import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.api.Git;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import java.io.IOException;

import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.EditList;
import org.eclipse.jgit.diff.HistogramDiff;
import org.eclipse.jgit.diff.RawText;
import org.eclipse.jgit.diff.RawTextComparator;

import java.io.File;
import java.util.ArrayList;
import static org.mockito.Mockito.*;
import java.util.List;

public class App{
	public static void main(String[] args) throws IOException, InvalidRemoteException, TransportException, 						GitAPIException {
		
		/*Inserción de los alumnos y las prácticas en la base de datos*/
		Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
      		Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
		Alumno maria = new Alumno("Maria", "Martin", "678954678X");
      		Alumno carmen = new Alumno("Carmen", "Guerra", "182739293F");
		Alumno pablo = new Alumno("Pablo", "Perez", "182738462H");
      		Alumno bea = new Alumno("Bea", "Alvarez", "182612828K");
		AlumnoDao daoAlum = new AlumnoDao();
		daoAlum.save(manolo);
		daoAlum.save(sergio);
		daoAlum.save(maria);
		daoAlum.save(carmen);
		daoAlum.save(pablo);
		daoAlum.save(bea);
		List<Alumno> allAlumnos = daoAlum.all();
		
		Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo-isi3", "123456789S");
		Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo-isi4", "234567897D");
		Practica practica3 = new Practica("https://gitlab.etsit.urjc.es/elenabal/repovacio", "678954678X");
		Practica practica4 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo_isi", "182739293F");
		Practica practica5 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo-isi2", "182738462H");
		Practica practica6 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo-isi1", "182612828K");
		PracticaDao daoPr = new PracticaDao();
		daoPr.save(practica1);
		daoPr.save(practica2);
		daoPr.save(practica3);
		daoPr.save(practica4);
		daoPr.save(practica5);
		daoPr.save(practica6);
		List<Practica> allPracticas = daoPr.all();
		
		for(int i=0;i<allPracticas.size();i+=4){  
			int j = i+1;
			
			//Crear los directorios donde se va a clonar
			String directory1 = "/tmp/prueba"+i+"a";
			String directory2 = "/tmp/prueba"+i+"b";
			
			//Indicamos los alumnos a comparar
			System.out.println("\n");
			System.out.println("##### DETECCIÓN DE COPIA #####\n");
			System.out.println("Alumnos que vamos a investigar: \n");
			System.out.println("Alumno 1:"+ allAlumnos.get(i).getNombre() + " " + allAlumnos.get(i).getApellidos());
			System.out.println("Alumno 2:"+ allAlumnos.get(j).getNombre() + " " + allAlumnos.get(j).getApellidos());
			
			//Obtener las urls que se quieren clonar
			String urlAlum1 = Practica.searchUrl(allAlumnos.get(i).getDni(), allPracticas, allAlumnos);
			String urlAlum2 = Practica.searchUrl(allAlumnos.get(j).getDni(), allPracticas, allAlumnos);
			
			//Clonar
			System.out.println("\n");
			System.out.println("##### ANALISIS DE LOS REPOSITORIOS #####\n");
			
			Git git = DetectorCopias.clonar(urlAlum1,directory1);
		   	Git git2 = DetectorCopias.clonar(urlAlum2,directory2);
		   	
		   	//Comprobar número de commits de cada repositorio
		   	int ncommits = DetectorCopias.contarCommits(git);
		   	System.out.println("En el primer repositorio hay " + ncommits + " commits \n");
		   	int ncommits2 = DetectorCopias.contarCommits(git2);
		   	System.out.println("En el segundo repositorio hay " + ncommits2 + " commits \n");
		   	
		   	//Obtener cuál es el último commit de cada repositorio
		   	String ultimocommit = DetectorCopias.getLastCommit(git);
		   	System.out.println("El último commit del respositorio 1 es: " + ultimocommit + "\n");
		   	
		   	String ultimocommit2 = DetectorCopias.getLastCommit(git2);
		   	System.out.println("El último commit del respositorio 2 es: " + ultimocommit2 + "\n");
		   	
		   	File directorio1 = new File(directory1);
			File directorio2 = new File(directory2);
			boolean copia = false;
			int n = 0;
			
			ArrayList<File> files = new ArrayList<File>();
			files = DetectorCopias.listarFicheros(directorio1);
			
			ArrayList<File> files2 = new ArrayList<File>();
			files2 = DetectorCopias.listarFicheros(directorio2);
			
			String fichero1 = null;
			String fichero2 = null;
			
			//System.out.println("Los ficheros del directorio son:");
			for(int k=0;k<files.size();k++){  	
		       	System.out.println("** Analisis fichero **");
				n = DetectorCopias.contarLineas(files.get(k));
				System.out.println("Numero de lineas totales del fichero: " + n);
				copia=DetectorCopias.difLineas(files.get(k), files2.get(k));
				System.out.println("\n");
				System.out.println("#### INFORME DE DIFERENCIAS #####");
				DetectorCopias.getDiff(files.get(k),files2.get(k));
			}
		}
	}
}
