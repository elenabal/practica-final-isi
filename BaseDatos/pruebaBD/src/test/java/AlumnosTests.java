import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import java.util.Arrays;
import java.util.List;

public class AlumnosTests{
	
	//Test para probar que se insertan alumnos correctamente 
	@Test
	public void insertarAlumnos() {
	      	Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");

		AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(manolo, sergio);
		when(dao.all()).thenReturn(results);

		assertEquals(manolo, results.get(0)); //Mete Manolo en la primera posición
		assertEquals(sergio, results.get(1)); //Mete Sergio en la segunda posición
    	}
    	
    	/*Tests para comprobar si un alumno está en la base de datos*/
    	//Test para buscar al alumno de la primera posición
    	@Test
    	public void buscarAlumnoEsta1(){
	    	Alumno paca = new Alumno("Paca", "Martinez", "347272828X");
	    	Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
	      	
	      	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(paca,manolo, sergio);
		when(dao.all()).thenReturn(results);
		
		List<Alumno> allAlumnos = dao.all();
		
		assertEquals(true, Alumno.searchAlumno("Paca", allAlumnos));
    	}
    	
    	//Test para buscar al alumno de una posición del medio
    	@Test
    	public void buscarAlumnoEsta2(){
	    	Alumno paca = new Alumno("Paca", "Martinez", "347272828X");
	    	Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
	      	
	      	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(paca,manolo, sergio);
		when(dao.all()).thenReturn(results);
		
		List<Alumno> allAlumnos = dao.all();
		
		assertEquals(true, Alumno.searchAlumno("Manolo", allAlumnos));
    	}
    	
    	//Test para buscar al alumno de la última posición
    	@Test
    	public void buscarAlumnoEsta3(){
	    	Alumno paca = new Alumno("Paca", "Martinez", "347272828X");
	    	Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
	      	
	      	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(paca,manolo, sergio);
		when(dao.all()).thenReturn(results);
		
		List<Alumno> allAlumnos = dao.all();
		
		assertEquals(true, Alumno.searchAlumno("Sergio", allAlumnos));
    	}
    	
    	//Test para comprobar cuando el alumno buscado no está en la base de datos
    	@Test
    	public void buscarAlumnoNoEsta(){
	    	Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
	      	
	      	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(manolo, sergio);
		when(dao.all()).thenReturn(results);
		List<Alumno> allAlumnos = dao.all();
		
		assertEquals(false, Alumno.searchAlumno("Lorena", allAlumnos));
    	}
    	
    	//Test para buscar en una base de datos con un solo elemento
    	@Test
    	public void baseDeDatosUnElemento(){
	    	Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	
	      	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(manolo);
		when(dao.all()).thenReturn(results);
		List<Alumno> allAlumnos = dao.all();
		
		assertEquals(true, Alumno.searchAlumno("Manolo", allAlumnos));
		assertEquals(false, Alumno.searchAlumno("Lorena", allAlumnos));
    	}
    	
    	//Test para buscar en una base de datos vacía
    	@Test
    	public void baseDeDatosVacia(){
	    	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList();
		when(dao.all()).thenReturn(results);
		List<Alumno> allAlumnos = dao.all();
		
		assertEquals(false, Alumno.searchAlumno("Lorena", allAlumnos));
    	}
    	
    	//Test para buscar en una base de datos NULL
    	@Test(expected = NullPointerException.class)
    	public void baseDeDatosNull(){
	    	AlumnoDao dao = mock(AlumnoDao.class);

		List<Alumno> results = Arrays.asList(null);
		when(dao.all()).thenReturn(results);
		List<Alumno> allAlumnos = dao.all();
    	}
}
