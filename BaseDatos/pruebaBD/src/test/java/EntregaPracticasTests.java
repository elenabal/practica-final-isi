import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import java.util.Arrays;
import java.util.List;

public class EntregaPracticasTests{
	//Tests para buscar en la base de datos la práctica del alumno que está en primera posición
	@Test
	public void buscarAlumno1() {
		//lista de alumnos en la base de datos
		Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
		Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");

		AlumnoDao daoAlum = mock(AlumnoDao.class);

		List<Alumno> alumnos = Arrays.asList(manolo, sergio);
		when(daoAlum.all()).thenReturn(alumnos);

		//lista de prácticas en la base de datos
		Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
		Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");

		PracticaDao daoPract = mock(PracticaDao.class);

		List<Practica> practicas = Arrays.asList(practica1, practica2);
		when(daoPract.all()).thenReturn(practicas);

		assertEquals(practica1.getUrl(), Practica.searchUrl("123456789S", practicas, alumnos));	
    	}
    	
    	//Tests para buscar en la base de datos la práctica del alumno que está en el medio
	@Test
	public void buscarAlumno2() {
		//lista de alumnos en la base de datos
		Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
	      	Alumno sara = new Alumno("Sara", "Perez", "254567897Y");

		AlumnoDao daoAlum = mock(AlumnoDao.class);

		List<Alumno> alumnos = Arrays.asList(manolo, sergio, sara);
		when(daoAlum.all()).thenReturn(alumnos);
		
		//lista de prácticas en la base de datos
	      	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");
		Practica practica3 = new Practica("https://gitlfgjghjab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "254567897Y");
		
		PracticaDao daoPract = mock(PracticaDao.class);

		List<Practica> practicas = Arrays.asList(practica1, practica2, practica3);
		when(daoPract.all()).thenReturn(practicas);
		
		assertEquals(practica2.getUrl(), Practica.searchUrl("234567897D", practicas, alumnos));	
    	}
    	
    	//Tests para buscar en la base de datos la práctica del alumno que está en la posición final
	@Test
	public void buscarAlumno3() {
		//lista de alumnos en la base de datos
		Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");

		AlumnoDao daoAlum = mock(AlumnoDao.class);

		List<Alumno> alumnos = Arrays.asList(manolo, sergio);
		when(daoAlum.all()).thenReturn(alumnos);
		
		//lista de prácticas en la base de datos
	      	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");

		PracticaDao daoPract = mock(PracticaDao.class);

		List<Practica> practicas = Arrays.asList(practica1, practica2);
		when(daoPract.all()).thenReturn(practicas);
		
		assertEquals(practica2.getUrl(), Practica.searchUrl("234567897D", practicas, alumnos));	
    	}
    	
    	//Test para buscar en una lista de alumnos vacía
    	@Test
	public void listaAlumnosVacia() {
		AlumnoDao daoAlum = mock(AlumnoDao.class);

		List<Alumno> alumnos = Arrays.asList();
		when(daoAlum.all()).thenReturn(alumnos);
		
		//lista de prácticas en la base de datos
	      	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");

		PracticaDao daoPract = mock(PracticaDao.class);

		List<Practica> practicas = Arrays.asList(practica1, practica2);
		when(daoPract.all()).thenReturn(practicas);
		
		assertEquals(null, Practica.searchUrl("234567897D", practicas, alumnos));	
    	}
    	
    	//Test para buscar en una lista de prácticas vacía
    	@Test
	public void listaPracticasVacia() {
		Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
	      	Alumno sergio = new Alumno("Sergio", "Perez", "234567897D");
		
		AlumnoDao daoAlum = mock(AlumnoDao.class);

		List<Alumno> alumnos = Arrays.asList(manolo, sergio);
		when(daoAlum.all()).thenReturn(alumnos);

		PracticaDao daoPract = mock(PracticaDao.class);

		List<Practica> practicas = Arrays.asList();
		when(daoPract.all()).thenReturn(practicas);
		
		assertEquals(null, Practica.searchUrl("234567897D", practicas, alumnos));	
    	}
    	
    	//Test para buscar cuando las dos listas están vacías
    	@Test
	public void listasVacias() {
		AlumnoDao daoAlum = mock(AlumnoDao.class);

		List<Alumno> alumnos = Arrays.asList();
		when(daoAlum.all()).thenReturn(alumnos);

		PracticaDao daoPract = mock(PracticaDao.class);

		List<Practica> practicas = Arrays.asList();
		when(daoPract.all()).thenReturn(practicas);
		
		assertEquals(null, Practica.searchUrl("234567897D", practicas, alumnos));	
    	}
    	
}
