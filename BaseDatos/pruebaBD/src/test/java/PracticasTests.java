import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import java.util.Arrays;
import java.util.List;

public class PracticasTests{
	
	//Test para probar que se insertan prácticas correctamente 
	@Test
	public void insertarEntregas() {
	      	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");

		PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1, practica2);
		when(dao.all()).thenReturn(results);

		assertEquals(practica1, results.get(0)); //Mete la práctica 1 en la primera posición
		assertEquals(practica2, results.get(1)); //Mete la práctica 2 en la segunda posición
    	}
    	
    	/*Tests para comprobar si una práctica está en la base de datos*/
    	//Test para buscar la práctica que está en la primera posición
    	@Test
    	public void buscarPracticaEsta1(){
	    	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");
	      	Practica practica3 = new Practica("https://gitlab.etsit.urjc.es/llorenzo/django-cms-css", "123459889S");
	      	
	      	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1, practica2, practica3);
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
		
		assertEquals(true, Practica.searchPractica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", allPracticas));
    	}
    	
    	//Test para buscar una práctica que está en una posición del medio
    	@Test
    	public void buscarPracticaEsta2(){
	    	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");
	      	Practica practica3 = new Practica("https://gitlab.etsit.urjc.es/llorenzo/django-cms-css", "123459889S");
	      	
	      	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1, practica2, practica3);
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
		
		assertEquals(true, Practica.searchPractica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", allPracticas));
    	}
    	
    	//Test para buscar una práctica que está en la última posición
    	@Test
    	public void buscarPracticaEsta3(){
	    	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");
	      	Practica practica3 = new Practica("https://gitlab.etsit.urjc.es/llorenzo/django-cms-css", "123459889S");
	      	
	      	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1, practica2, practica3);
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
		
		assertEquals(true, Practica.searchPractica("https://gitlab.etsit.urjc.es/llorenzo/django-cms-css", allPracticas));
    	}
    	
    	//Test para comprobar cuando una práctica no está en la base de datos
    	@Test
    	public void buscarPracticaNoEsta(){
	    	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");
	      	Practica practica3 = new Practica("https://gitlab.etsit.urjc.es/llorenzo/django-cms-css", "123459889S");
	      	
	      	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1, practica2, practica3);
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
		
		assertEquals(false, Practica.searchPractica("https://gitlab.etsit.urjc.es/lllorenzo/django-cms-css", allPracticas));
    	}
    	
    	//Test para buscar en una base de datos con un solo elemento
    	@Test
    	public void baseDeDatosUnElemento(){
	    	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	
	      	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1);
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
		
		assertEquals(true, Practica.searchPractica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", allPracticas));
		assertEquals(false, Practica.searchPractica("https://gitlab.etsit.urjc.es/elenaabal/django-cms-css", allPracticas));
    	}
    	
    	//Test para buscar en una base de datos vacía
    	@Test
    	public void baseDeDatosVacia(){
	    	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList();
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
		
		assertEquals(false, Practica.searchPractica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", allPracticas));
    	}
    	
    	//Test para buscar en una base de datos NULL
    	@Test(expected = NullPointerException.class)
    	public void baseDeDatosNull(){
	    	PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(null);
		when(dao.all()).thenReturn(results);
	      	
		List<Practica> allPracticas = dao.all();
    	}
    	
    	//Test para buscar la url de una práctica con el dni de un alumno
    	@Test
	public void buscarPracticaAlumno() {
		Alumno manolo = new Alumno("Manolo", "Martinez", "123456789S");
		AlumnoDao daoAl = mock(AlumnoDao.class);
		List<Alumno> resultsAl = Arrays.asList(manolo);
		when(daoAl.all()).thenReturn(resultsAl);
		List<Alumno> allAlumnos = daoAl.all();        
		
	      	Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	      	Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");

		PracticaDao dao = mock(PracticaDao.class);

		List<Practica> results = Arrays.asList(practica1, practica2);
		when(dao.all()).thenReturn(results);
		List<Practica> allPracticas = dao.all();

		assertEquals("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", Practica.searchUrl(allAlumnos.get(0).getDni(), allPracticas, allAlumnos));
    	}
}
