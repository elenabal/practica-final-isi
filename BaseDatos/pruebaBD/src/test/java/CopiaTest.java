import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.*;
import java.io.File;
import java.io.*;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;


public class CopiaTest{
	private Practica practica1 = new Practica("https://gitlab.etsit.urjc.es/elenabal/ejercicios-intro-pruebas", "234567897D");
	private Practica practica2 = new Practica("https://gitlab.etsit.urjc.es/elenabal/django-cms-css", "123456789S");
	private Practica practica3 = new Practica("https://gitlab.etsit.urjc.es/elenabal/repovacio", "789564321S");
	private Practica practica4 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo_isi", "543219876F");
	private Practica practica5 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo-isi2", "121314151H");
	private Practica practica6 = new Practica("https://gitlab.etsit.urjc.es/jffuncia/pruebas-repo-isi1", "313233345O");  
        
	// Test para comprobar que el número de commits obtenidos de una práctica es correcto
	@Test
	public void countCommits() throws IOException, InvalidRemoteException, TransportException,GitAPIException {
		int ncommits = 8;
		Git git = DetectorCopias.clonar(practica1.getUrl(), "/tmp/c4");
		assertEquals(ncommits, DetectorCopias.contarCommits(git));
	}
	
	// Test para comprobar que se compara correctamente el contenido de dos ficheros
	@Test
	public void compareFiles() throws IOException, InvalidRemoteException, TransportException,GitAPIException {
		Git git1 = DetectorCopias.clonar(practica5.getUrl(), "/tmp/a6");
		File directory1 = new File("/tmp/a6/prueba.txt");
		Git git2 = DetectorCopias.clonar(practica6.getUrl(), "/tmp/b6");
		File directory2 = new File("/tmp/b6/prueba.txt");
		assertEquals(false, DetectorCopias.difLineas(directory1, directory2));
	}
	
	// Test para comprobar que una entrega está vacía
	@Test
	public void emptyPractica()  throws IOException, InvalidRemoteException, TransportException,GitAPIException {
		File directory = new File("/tmp/prueba16");
		assertEquals(null, DetectorCopias.listarFicheros(directory));
	}
	
	// Test para comprobar si devuelve bien el último commit (al que apunta HEAD)
	@Test
	public void getLastComm() throws IOException, InvalidRemoteException, TransportException,GitAPIException {
		String commit = "commit 821b4a660b212de177a21650f361510ca2c40764 1638176276 ----sp";
		Git git = DetectorCopias.clonar(practica1.getUrl(), "/tmp/getLastCommiitt");
		assertEquals(commit, DetectorCopias.getLastCommit(git));
	}
	
	// Test para comprobar que el fichero no está vacio (tiene más de 0 lineas)
	@Test
	public void countlines() throws IOException, InvalidRemoteException, TransportException,GitAPIException {
		int nlines = 0;
		Git git = DetectorCopias.clonar(practica4.getUrl(), "/tmp/counttllines");
		File directory = new File("/tmp/counttllines/prueba.txt");
		assertEquals(nlines, DetectorCopias.contarLineas(directory));
	}
}
