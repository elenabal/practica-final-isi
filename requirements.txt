## REQUISITOS

## REQUISITOS FUNCIONALES:

* Corrección automática de las prácticas. (Como un profesor, para poder corregir las prácticas más rápido, quiero tener corrección automática).

* Detección de copias (Como un profesor, para que la evaluación sea objetiva, quiero tener un detector de copias para que cuando se entregue una solución a una práctica se detecten si se ha copiado o no con otras entregas de compañeros).

* Listado de notas. (Como un profesor, para que pueda conocer todas las notas de los alumnos, quiero un listado de notas).

* Informe de plagios. (Como un directivo, para que no haya plagios en mi universidad, quiero un informe de plagios. ← Nuevo stakeholdes --> Directivos.)

* Notas proporcionales al numero de test que pase la entrega. (Como profesor, para que la evaluación sea acorde a los conocimientos, quiero que la nota sea proporcional al número de test que supere la entrega).

* Borradores para las entregas. (Como alumno, para que pueda editar mi práctica antes de entregarla, quiero poder dejarla en borrador hasta la entrega definitiva)

* Borradores para las entregas. (Como profesor, para que pueda detectar plagio entre alumnos, quiero borradores para poder saber quién ha plagiado a quién)

* Cada alumno puede solo puede ver sus propias notas. (Como directivo, para que los gobiernos no me acusen de violentar la ley de privacidad, quiero que cada alumno solo pueda ver sus notas).

* Tener en cuenta calidad/limpieda del codigo. (Como profesor, para que pueda corregir mejor las prácticas, quiero que también se tenga en cuenta la calidad/limpieza del código (en este caso el stackeholdes es el profesor)).

* Comentarios en las entregas.(Como profesor, para que mis alumnos reciban feedback de su nota, quiero porder añadir comentarios al respecto).

* Formato del nombre de la practica de una manera especifica. (Como profesor , para que sea más fácil realizar la corrección, quiero que el formato de nombre de la práctica sea de una manera específica).


## REQUISITOS NO FUNCIONALES:

* CI/CD (es un requisito de contexto, ya  que el desarrollo se debe realizar mediante CD/CI).

* Implementación mediante java.

* Utilización de bases de datos.

* Se tienen que escribir test como conectores  → Correción automática con test.
